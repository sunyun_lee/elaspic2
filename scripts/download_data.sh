#!/bin/bash

pushd src/elaspic2/plugins/alphafold/data/params
curl -fsSL https://storage.googleapis.com/alphafold/alphafold_params_2021-07-14.tar | tar -x
popd

