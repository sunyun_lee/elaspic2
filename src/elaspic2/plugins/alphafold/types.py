from typing import Any, Dict, List, NamedTuple, Optional


class AlphaFoldData(NamedTuple):
    sequence: str
    ligand_sequence: Optional[str]
    msa: List[str]
    predictions: Dict[str, Any]
