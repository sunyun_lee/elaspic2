import logging
import subprocess
from pathlib import Path
from typing import Any, Dict, List, Optional, Union

import haiku as hk
import jax
import numpy as np
from alphafold.data import pipeline, templates
from alphafold.model import config, data, model
# =========================> 
from alphafold.data.templates import SingleHitResult
import datetime, logging, os
# <=========================
from kmtools.structure_tools.types import DomainMutation as Mutation

import elaspic2.plugins.alphafold.data
from elaspic2.core import MutationAnalyzer, SequenceTool
from elaspic2.plugins.alphafold.types import AlphaFoldData

logger = logging.getLogger(__name__)


class PatchedRunModel(model.RunModel):
    def __init__(self, config, params=None):
        self.config = config
        self.params = params

        def _forward_fn(batch):
            alphafold_model = model.modules.AlphaFold(self.config.model)
            return alphafold_model(
                batch,
                is_training=False,
                compute_loss=False,
                ensemble_representations=True,
                return_representations=True,
            )

        self.apply = jax.jit(hk.transform(_forward_fn).apply)
        self.init = jax.jit(hk.transform(_forward_fn).init)


class AlphaFold(SequenceTool, MutationAnalyzer):
    model_params = None
    model_config = None
    model = None
    is_loaded: bool = False

    @classmethod
    def load_model(cls, model_name="model_1", heads_to_remove=(), device=None) -> None:
        if device not in ["cpu", "gpu", "tpu"]:
            raise ValueError(f"Unsupported device type: {device!r}.")
        jax.config.update("jax_platform_name", device)

        data_dir = cls._get_data_dir()
        cls._download_model_data(data_dir)

        model_params = data.get_model_haiku_params(
            model_name=model_name + "_ptm", data_dir=str(data_dir)
        )
        cls.model_params = model_params

        model_config = config.model_config(model_name + "_ptm")
        model_config.data.eval.num_ensemble = 1
        model_config.model.num_recycle = 0  # AS
        for head in heads_to_remove:
            del model_config.model.heads[head]
        cls.model_config = model_config

        cls.model = PatchedRunModel(model_config, model_params)

        cls.is_loaded = True

    @staticmethod
    def _get_data_dir():
        return Path(elaspic2.plugins.alphafold.data.__path__[0])

    @classmethod
    def _download_model_data(cls, data_dir: Optional[Path] = None):
        # This helps when downloading module data while building the Docker image
        if data_dir is None:
            data_dir = cls._get_data_dir()

        data_dir.mkdir(exist_ok=True)

        required_files = [
            data_dir.joinpath("params", f"params_model_{i}_ptm.npz") for i in range(1, 6)
        ] + [data_dir.joinpath("params", f"params_model_{i}.npz") for i in range(1, 6)]

        if not all([file.is_file() for file in required_files]):
            logger.info("Downloading AlphaFold model files. This may take several minutes...")
            url = "https://storage.googleapis.com/alphafold/alphafold_params_2021-07-14.tar"
            system_command = f"curl -fsSL {url} | tar -x"
            subprocess.run(system_command, shell=True, cwd=data_dir.joinpath("params"), check=True)

# ==================================================================
# ==================================================================
# =========================> 
    @staticmethod 
    def _get_template_with_query_structure(
        query_sequence: str, 
        template_sequence: str, 
        query_pdb_code: str, 
        query_chain_id: str,
        mapping: Dict[int, int],
        cif_path: str, 
        max_template_date: datetime.datetime,
        strict_error_check: bool = False
    ) -> SingleHitResult:
        """ Returns a template given query sequence's protein structure 
        """
        # Error check: keys and values in mapping must be identical 
        for key in mapping: 
            if mapping[key] != key: 
                error = "Mapping should have identical indices"
                return SingleHitResult(features=None, error=error, warning=None)
        
        logging.info("Reading PDB entry from %s. Query: %s", cif_path, query_sequence)

        with open(cif_path, "r") as cif_file: 
            cif_string = cif_file.read()
        
        parsing_result = mmcif_parsing.parse(
            file_id = query_pdb_code, 
            mmcif_string = cif_string
        )
        mmcif_object = parsing_result.mmcif_object

        # Error check: query sequence should be subsequence of the corresponding chain 
        # sequence without gaps at the beginning (if not `_find_template_in_pdb` will look for 
        # the query sequence in other chain or make "fuzzy sequence" 
        # if the sequence does not exist in the template)
        chain_sequence = mmcif_object.chain_to_seqres[query_chain_id]
        if chain_sequence.find(query_sequence) != 0: 
            error = "Could not find query sequence in template"
            return SingleHitResult(features=None, error=error, warning=None)
        
        # Error check: release date doesn't exceed max template date 
        if parsing_result.mmcif_object is not None: 
            mmcif_release_date = datetime.datetime.strptime(
                parsing_result.mmcif_object.header["release_date"], "%Y-%m-%d"
            )
            if mmcif_release_date > max_template_date: 
                error = ("Template %s date (%s) > max template date (%s)."  % 
                (query_pdb_code, mmcif_release_date, max_template_date)) 
                if strict_error_check: 
                    return SingleHitResult(features=None, error=error, warning=None)
                else: 
                    logging.warning(error)
                    return SingleHitResult(features=None, error=None, warning=None)
        
        try:
            features, realign_warning = templates._extract_template_features(
                mmcif_object = parsing_result.mmcif_object,
                pdb_id = query_pdb_code, 
                mapping = mapping,
                template_sequence = template_sequence, 
                query_sequence = query_sequence,
                template_chain_id = query_chain_id,
                kalign_binary_path = "/DONOTACCESS"
            )
            features["template_sum_probs"] = [len(query_sequence)]

            return SingleHitResult(features=features, error=None, warning=realign_warning)
        
        except (NoChainsError, NoAtomDataInTemplateError, TemplateAtomMaskAllZerosError) as e:
            warning = ('%s_%s : feature extracting errors: %s, mmCIF parsing errors: %s' 
                    % (query_pdb_code, query_chain_id, str(e), parsing_result.errors))
            
            if strict_error_check:
                return SingleHitResult(features=None, error=warning, warning=None)
            else:
                return SingleHitResult(features=None, error=None, warning=warning)

        except Error as e: 
            error = ('%s_%s : feature extracting errors: %s, mmCIF parsing errors: %s' 
                    % (query_pdb_code, query_chain_id, str(e), parsing_result.errors))
            return SingleHitResult(features=None, error=error, warning=None)

    @staticmethod
    def _mk_template_with_query_structure(
        query_sequence: str, 
        template_sequence: str, 
        query_name: str, 
        max_template_date: datetime.datetime,
        cif_path: str, 
        query_release_date: Optional[datetime.datetime] = None, 
        strict_error_check: bool = False) -> Dict[str, List[np.object]]:
        """ Returns features for a single template of query sequence's protein structure 
        Combination of TemplateHitFeaturizer.__init__ and TempalteHitFeaturizer.get_templates
        """
        # Error check: the cif file exists
        if not (cif_path.endswith(".cif") and os.path.isfile(cif_path)):
            raise ValueError("Could not find CIF file %s" % cif_path)
        
        template_cutoff_date = max_template_date
        if query_release_date: 
            delta = datetime.timedelta(days=60)
            if query_release_date - delta < template_cutoff_date:
                template_cutoff_date = query_release_date - delta
            assert template_cutoff_date < query_release_date
        assert template_cutoff_date <= max_template_date

        mapping = {i:i for i in range(len(query_sequence))}

        # Split query_name into pdb_code pdb id and chain id
        # Source: https://github.com/deepmind/alphafold/blob/1d43aaff941c84dc56311076b58795797e49107b/alphafold/data/templates.py#L100
        id_match = re.match(r'[a-zA-Z\d]{4}_[a-zA-Z0-9.]+', query_name)
        if not id_match:
            raise ValueError(f'query_name did not start with PDBID_chain: {query_name}')
        pdb_id, chain_id = id_match.group(0).split('_')
        pdb_id = pdb_id.lower()

        result = _get_template_with_query_structure(
                    template_sequence = template_sequence,
                    query_sequence = query_sequence,
                    query_pdb_code = pdb_id,
                    query_chain_id = chain_id,
                    mapping = mapping,
                    cif_path = cif_path,
                    max_template_date = template_cutoff_date,
                    strict_error_check = strict_error_check,
                )

        template_features = {}
        for k in result.features: 
            template_features[k] = np.array(result.features[k])[None]

        return template_features



# <=========================
# ==================================================================
# ==================================================================

    @classmethod
    def build(  # type: ignore[override]
        cls,
        sequence: str,
        ligand_sequence: Optional[str],
        msa: List[str],
        # =======>
        cif_path: Optional[str] = None,
        query_name: Optional[str] = None, # "PDBid_chain" format where PDBid is 4 letter alphabet and chain is alphanumeral with no length limit 
        # <=======
        remove_hetatms=True,
    ) -> AlphaFoldData:
        if ligand_sequence is not None:
            raise NotImplementedError("Interactions are not yet implemented.")

        if remove_hetatms:
            sequence = sequence.replace("X", "")

        # ================>
        if cif_path and query_name: 
            today = datetime.datetime.today()
            max_template_date = today.repace(year=today.year+100)
            return processed_feature_dict = AlphaFold._mk_template_with_query_structure(
                query_sequence=sequence,
                template_sequence = sequence,
                query_name = query_name,
                max_template_date = max_template_date,
                cif_path = cif_path
                )
        elif cif_path is None or query_name is None: 
            raise ValueError("Both cif_path and query_name arguments are required.")
        else: 
        # <================
            processed_feature_dict = AlphaFold._create_feature_dict(sequence, msa)

        predictions = AlphaFold.model.predict(processed_feature_dict)

        return AlphaFoldData(
            sequence=sequence, ligand_sequence=ligand_sequence, msa=msa, predictions=predictions
        )

    @classmethod
    def analyze_mutation(cls, mutation: str, data: AlphaFoldData) -> dict:
        mut = Mutation.from_string(mutation)

        if data.sequence[int(mut.residue_id) - 1] != mut.residue_wt:
            raise AlphaFoldAnalyzeError(
                f"Mutation does not match sequence ({mut}, {data.sequence})."
            )

        def mutate_sequence(sequence, mut):
            aa_list = list(sequence)
            assert aa_list[mut.residue_id - 1] == mut.residue_wt
            aa_list[mut.residue_id - 1] = mut.residue_mut
            sequence_mut = "".join(aa_list)
            assert sequence != sequence_mut
            return sequence_mut

        sequence_mut = mutate_sequence(data.sequence, mut)
        processed_feature_dict_mut = cls._create_feature_dict(sequence_mut, data.msa)
        predictions_mut = AlphaFold.model.predict(processed_feature_dict_mut)

        results_wt = {
            f"{key}_wt": value
            for key, value in cls._predictions_to_embeddings(
                data.predictions, mut.residue_id - 1
            ).items()
        }
        results_mut = {
            f"{key}_mut": value
            for key, value in cls._predictions_to_embeddings(
                predictions_mut, mut.residue_id - 1
            ).items()
        }
        return results_wt | results_mut

    @classmethod
    def _create_feature_dict(cls, sequence, msa, random_seed=0) -> Dict[str, Any]:
        msa, deletion_matrix = pipeline.parsers.parse_a3m("".join(msa))
        feature_dict = {
            **pipeline.make_sequence_features(
                sequence=sequence, description="none", num_res=len(sequence)
            ),
            **pipeline.make_msa_features(msas=[msa], deletion_matrices=[deletion_matrix]),
            **cls._make_mock_template(len(sequence)),
        }
        processed_feature_dict = cls.model.process_features(feature_dict, random_seed=random_seed)
        return processed_feature_dict

    @staticmethod
    def _make_mock_template(size):
        # Adapted from: https://github.com/sokrypton/ColabFold
        output_templates_sequence = "-" * size
        output_confidence_scores = np.full(size, -1)
        templates_all_atom_positions = np.zeros(
            (size, templates.residue_constants.atom_type_num, 3)
        )
        templates_all_atom_masks = np.zeros((size, templates.residue_constants.atom_type_num))
        templates_aatype = templates.residue_constants.sequence_to_onehot(
            output_templates_sequence, templates.residue_constants.HHBLITS_AA_TO_ID
        )
        template_features = {
            "template_all_atom_positions": templates_all_atom_positions[None],
            "template_all_atom_masks": templates_all_atom_masks[None],
            "template_sequence": [b"none"],
            "template_aatype": np.array(templates_aatype)[None],
            "template_confidence_scores": output_confidence_scores[None],
            "template_domain_names": [b"none"],
            "template_release_date": [b"none"],
        }
        return template_features

    @staticmethod
    def _predictions_to_embeddings(predictions, idx: int) -> Dict[str, Union[float, np.ndarray]]:
        assert idx >= 0

        def as_residue(x):
            return x[idx].to_py()

        def as_protein(x):
            return x.mean(axis=0).to_py()

        embeddings = {
            "experimentally_resolved": predictions["experimentally_resolved"]["logits"],
            "predicted_lddt": predictions["predicted_lddt"]["logits"],
            "msa_first_row": predictions["representations"]["msa_first_row"],
            "single": predictions["representations"]["single"],
            "structure_module": predictions["representations"]["structure_module"],
        }

        output = {
            "scores_residue_plddt": predictions["plddt"][idx].item(),
            "scores_protein_plddt": predictions["plddt"].mean(axis=0).item(),
            "scores_protein_max_predicted_aligned_error": predictions[
                "max_predicted_aligned_error"
            ].item(),
            "scores_proten_ptm": predictions["ptm"].item(),
            **{f"features_residue_{key}": as_residue(value) for key, value in embeddings.items()},
            **{f"features_protein_{key}": as_protein(value) for key, value in embeddings.items()},
        }

        return output


class AlphaFoldBuildError(Exception):
    pass


class AlphaFoldAnalyzeError(Exception):
    pass
