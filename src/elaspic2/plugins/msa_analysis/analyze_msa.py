import shutil
import subprocess
import tempfile
from collections import Counter
from pathlib import Path

import numpy as np
import pandas as pd

import elaspic2.plugins.msa_analysis.data

AMINO_ACIDS = "ARNDCEQGHILKMFPSTWYV"


def alignment_to_sequences(alignment):
    sequences = [
        "".join((aa for aa in line.strip() if not aa.islower()))
        for line in alignment
        if line and not line.startswith(">")
    ]
    return sequences


def sequences_to_counts(sequences):
    counts_mat = np.zeros((len(sequences[0]), len(AMINO_ACIDS)), dtype=np.float64)
    for i, msa_row in enumerate(zip(*sequences)):
        msa_row = [aa for aa in msa_row if aa in AMINO_ACIDS]
        counts = Counter(msa_row)
        counts_mat[i, :] = [counts.get(aa, 0) for aa in AMINO_ACIDS]
    return counts_mat


def counts_to_probas(counts_mat):
    probas_mat = np.log((counts_mat + 1) / (counts_mat.sum(axis=1, keepdims=True) + 20))
    return probas_mat


def run_convervation_script(sequences):
    data_dir = Path(elaspic2.plugins.msa_analysis.data.__path__[0])
    script_name = "Conservation.jl"
    fasta_string = "".join((f">{i}\n{seq}\n" for i, seq in enumerate(sequences)))

    with tempfile.TemporaryDirectory() as tmp_dir:
        tmp_path = Path(tmp_dir)
        shutil.copy(data_dir.joinpath(script_name), tmp_path.joinpath(script_name))
        with tmp_path.joinpath("aln.fasta").open("wt") as fout:
            fout.write(fasta_string)
        cmd = ["julia", script_name, "-f", "FASTA", "aln.fasta"]
        _ = subprocess.run(
            cmd, cwd=tmp_dir, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True
        )
        result_df = pd.read_csv(tmp_path.joinpath("aln.fasta.conservation.csv"), comment="#")

    assert len(result_df) == len(sequences[0])
    return result_df


def featurize_mutation(
    mutation,
    *,
    sequence,
    msa_counts_mat,
    msa_probas_mat,
    msa_conservation_df,
    msa_length,
    msa_proba_mean,
):
    aa_wt, pos, aa_mut = mutation[0], int(mutation[1:-1]), mutation[-1]
    if len(sequence) < pos or sequence[pos - 1] != aa_wt:
        raise ValueError(f"Mutation {mutation!r} does not match sequence.")

    cons = msa_conservation_df.iloc[pos - 1]
    assert cons.i == pos

    idx_wt = AMINO_ACIDS.index(aa_wt)
    idx_mut = AMINO_ACIDS.index(aa_mut)
    msa_result = {
        "mut": mutation,
        "msa_count_wt": msa_counts_mat[pos - 1, idx_wt],
        "msa_count_mut": msa_counts_mat[pos - 1, idx_mut],
        "msa_count_total": msa_counts_mat[pos - 1].sum().item(),
        "msa_proba_wt": msa_probas_mat[pos - 1, idx_wt],
        "msa_proba_mut": msa_probas_mat[pos - 1, idx_mut],
        "msa_proba_total": msa_probas_mat[pos - 1].sum().item(),
        "msa_length": msa_length,
        "msa_proba": msa_proba_mean,
        "msa_H": cons.H,
        "msa_KL": cons.KL,
    }
    return msa_result


RESIDUE_ENCODING_WT = {
    "R": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0),
    "T": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0),
    "F": (0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "P": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0),
    "I": (0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "V": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0),
    "S": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0),
    "M": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "L": (0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "D": (0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "H": (0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "A": (1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "G": (0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "E": (0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "N": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0),
    "K": (0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "Q": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0),
    "Y": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
    "C": (0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "W": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0),
}


RESIDUE_ENCODING_MUT = {
    "C": (0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "P": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0),
    "S": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0),
    "T": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0),
    "H": (0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "M": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "V": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0),
    "L": (0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "I": (0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "G": (0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "Q": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0),
    "N": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0),
    "K": (0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "R": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0),
    "D": (0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "E": (0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "W": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0),
    "A": (1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "F": (0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    "Y": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
}
